Set-ExecutionPolicy Unrestricted

# Install Applications
choco install notepadplusplus
choco install GoogleChrome
choco install firefox
choco install cmder
choco install agentransack
choco install teamviewer
choco install baretail
choco install libreoffice-fresh
choco install filezilla
choco install heidisql
choco install sqlite
choco install sqlite-studio.portable
choco install netscan

# Install Utilities
choco install sysinternals
choco install procmon
choco install 7zip
choco install rdcman
choco install rdtabs
choco install logparser
choco install PowerShellGAC
choco install procexp
